$(document).ready(function() {
						   
	$("#smallheader").click().toggle(function(){
		 $("div#container").animate({top:'-22px'},'slow');
		 $("#header h1 a").animate({top:'50px'},'slow');
		 },function(){
	    $("div#container").animate({top:'0'},'slow');
		$("#header h1 a").animate({top:'40px'},'slow');
	});
	
	$("div.block h2").append("<span></span>");
	$("div.block h2").click().toggle(function(){
		 $(this).find("span").addClass("close");
		 $(this).next().slideUp("slow");
		 
		 },function(){
	     $(this).find("span").removeClass("close");
		 $(this).next().slideDown("slow");
		
	});
	
    $("div#header ul.menu li").hover(
        function(){ $("ul", this).fadeIn("slow"); }, 
        function() { });
    if (document.all) {
        $("div#header ul.menu li").hoverClass("sfHover");
    }	

	$.fn.hoverClass = function(c) {
		return this.each(function(){
			$(this).hover( 
				function() { $(this).addClass(c);  },
				function() { $(this).removeClass(c); }
			);
		});
	};    

});
