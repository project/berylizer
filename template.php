<?php

function menu_tree_full($menu_name = 'navigation') {
  static $menu_output = array();

  if (!isset($menu_output[$menu_name])) {
    $tree = menu_find_active_trail(menu_tree_all_data($menu_name));
    $menu_output[$menu_name] = menu_tree_output($tree);
  }
  return $menu_output[$menu_name];
}

/**
 * Wrapper function
 */
function menu_find_active_trail(&$menu_tree) {
  $item = menu_get_item();
  _menu_find_active_trail($menu_tree, $item);
  return $menu_tree;
}
/**
 * Recursive function to find the active menu and the active trail in the given tree.
 */
function _menu_find_active_trail(&$menu_tree, $item) {
  $level_is_expanded = FALSE;
  foreach($menu_tree as &$menu_item) {
    $link = &$menu_item['link'];
    if ($link['href']==$item['href']) { // Found the exact location in the tree
      $link['active'] = TRUE;
      $link['in_active_trail'] = TRUE;
      return true;
    } else {
      if ($link['has_children']) {
        $result = _menu_find_active_trail($menu_item['below'], $item);
        $link['in_active_trail'] = $result;
        if ($result) $level_is_expanded = TRUE;
      }
    }
  }
  return $level_is_expanded;
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode(' › ', $breadcrumb) .'</div>';
  }
}

/**
 * Allow themable wrapping of all comments.
 */
function phptemplate_comment_wrapper($content, $node) {
  if (!$content || $node->type == 'forum') {
    return '<div id="comments">'. $content .'</div>';
  }
  else {
    return '<div id="comments"><h2 class="comments">'. t('Comments') .'</h2>'. $content .'</div>';
  }
}

/**
 * Override or insert PHPTemplate variables into the templates.
 */
function phptemplate_preprocess_page(&$vars) {
  $vars['tabs2'] = menu_secondary_local_tasks();

  // Hook into color.module
  if (module_exists('color')) {
    _color_page_alter($vars);
  }
}

/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs. Overridden to split the secondary tasks.
 *
 * @ingroup themeable
 */
function phptemplate_menu_local_tasks() {
  return menu_primary_local_tasks();
}

function phptemplate_comment_submitted($comment) {
  return t('!datetime — !username',
    array(
      '!username' => theme('username', $comment),
      '!datetime' => format_date($comment->timestamp)
    ));
}

function phptemplate_node_submitted($node) {
  return t('!datetime — !username',
    array(
      '!username' => theme('username', $node),
      '!datetime' => format_date($node->created),
    ));
}
