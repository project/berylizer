<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php print $head_title ?></title>
<?php print $head ?>
<?php print $styles ?>
<?php print $scripts ?>
</head>

<body>

<div id="container"> 
  <div id="header">
  <div id="header-xtras">
  	<div class="content">
<?php global $user; ?>
<?php if ($user->uid) : ?>
<?php print(t('Logged in as:'))?> <?php print l($user->name,'user/'.$user->uid); ?> | <?php print l((t('Logout')),"logout"); ?>
<?php else : ?>
<?php endif; ?> |
<a href="/">Go to frontpage!</a>
    </div>
    <div id="smallheader" title="Minimize Header"><!-- --></div>
  </div>
  
  <?php
          // Prepare header
          $site_fields = array();
          if ($site_name) {
            $site_fields[] = check_plain($site_name);
          }
          if ($site_slogan) {
            $site_fields[] = check_plain($site_slogan);
          }
          $site_title = implode(' ', $site_fields);
          if ($site_fields) {
            $site_fields[0] = '<span>'. $site_fields[0] .'</span>';
          }
          $site_html = implode(' ', $site_fields);

          if ($logo || $site_title) {
            print '<h1><a href="'. check_url($base_path) .'" title="'. $site_title .'">';
            if ($logo) {
              print '<img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />';
            }
            print $site_html .'</a></h1>';
          }
        ?>
     <div id="navigation">
	 <?php print menu_tree("navigation"); ?>
     </div>
     <?php print $header; ?>
      

<div id="block-search-0" class="block block-search">
<?php print $search_box ?>
</div>
      
 <?php print $breadcrumb; ?>
  <!-- end #header -->
  </div>
  <div id="inner">
  
  <div id="sidebar">

    <div id="block-menu-primary-links" class="block  block-menu">
    <h2>Primary Links</h2>
	<?php if (is_array($primary_links)) : ?>
    <div class="content">
    <ul class="menu">
    <?php foreach ($primary_links as $link): ?>
    <li><?php
    
    $href = $link['href'] == "<front>" ? base_path() : base_path() . $link['href'];
    print "<a href='" . $href . "'>" . $link['title'] . "</a>";
    
    ?></li>
    
    <?php endforeach; ?>
    </ul>
    <?php endif; ?>
    </div>
    </div>
 
  <?php print $left ?>
  </div>
  
  <div id="mainContent">
  <?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
  <div class="inner">
		<?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
          <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul>'; endif; ?>
          <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
          <?php if ($show_messages && $messages): print $messages; endif; ?>
          <?php print $help; ?>
          
            <?php print $content ?>
  		</div>

</div>

<br class="clearfloat" />    
</div>

  <div id="footer">
  <p>designed by <a href="http://www.berylune.de" target="_blank">www.berylune.de</a> - 2009</p>
    <?php print $footer ?>
</div>

    
</div>

  <?php print $closure ?>
</body>
</html>
